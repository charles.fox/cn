#include "OptionHandler.hpp"

namespace cn
{

OptionHandler::OptionHandler(int argc, char *argv[])
{
    if (argc == 1)
    {
        log(Log::Critical) << "No arguments given." << std::endl;
        exit(2);
    }

    while (1)
    {
        char c = getopt_long(argc, argv, "o:vVh",
                             long_options, &optionIndex);

        if (c == -1)
            // Exhausted opts in argv, leave loop.
            break;

        switch (c)
        {
            case 'V':
                std::cout << VERSION_STRING << std::endl;
                exit(0);
            case 'v':
                // TODO Allow granular setting.
                GLOBAL_LEVEL = Log::Verbose;
                break;
            case 'o':
                _outputDir = std::string(optarg);
            case 0:
                // This case deals with all valid longopts without a short version.
                // If just a flag, write to variable (e.g. --preserve-dir).
                // If needed, struct entry is long_option[option_index].
                break;
            case 'h':
                std::cout << HELP_STRING << std::endl;
                exit(0);
            default:
                std::cout << HELP_STRING << std::endl;
                exit(2);
        }
    }

    optionIndex = optind;

    if (optionIndex >= argc)
    {
        log(Log::Critical) << "No files provided." << std::endl;
        exit(1);
    }
}

int OptionHandler::preserveDir()
{
    return _preserveDir;
}

std::string OptionHandler::outputDir()
{
    return _outputDir;
}

int OptionHandler::traceLexer()
{
    return _traceLexer;
}

int OptionHandler::traceParser()
{
    return _traceParser;
}

}
