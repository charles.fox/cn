#ifndef FILECONTEXT_H
#define FILECONTEXT_H

#include <string>
#include <fstream>
#include <stack>
#include <memory>

#include "ScopeInfo.hpp"
#include "OptionHandler.hpp"

#include "location.hh"

namespace cn
{

class FileContext
{
//private:
public:
    // TODO Encapsulate these variables and friend Parser class.
    location _location;
    OptionHandler &_opts;

    std::ofstream outHpp;
    std::ofstream outCpp;

    size_t indentWidth = 0;
    size_t indentPrev = 0;

    // scopeFloor represents the highest indentation at which the line in the
    // cn file should be written to the hpp file. For example, when scopeFloor
    // is 1, e.g. in a struct or class, members of the current scope will be
    // written to the header file, but their implementation will not. When we
    // leave that scope, the scopeFloor will lower to the global scope, 0.
    size_t scopeFloor = 0;
    // scopeFloorIncrement is used to determine if the scope floor should be
    // changed on the next line without altering the value of scopeFloor when
    // processing the current line.
    bool scopeFloorIncrement = false;

    // scopeInfoStack contains information useful in the underlying scope,
    // such as the string to close the scope with any string to write in front
    // of a definition in the cpp/implementation file. This is primarily for
    // the sake of classes, whose prefix is "{ClassName}::" and must close
    // with "};".
    std::stack<std::unique_ptr<ScopeInfo>> scopeInfoStack;

    std::unique_ptr<ScopeInfo> prevExprScopeInfo;

    // When the current indentation level == scopeFloor, we do not know if we
    // should write a line to the implementation file until the next line, so
    // we store it here. For example, a function declaration on its own should
    // only be written to the header file, but a function definition should be
    // written to the implementation file and its declaration to the header.
    std::string prevExpr = "";

    bool selfIncludeFlag;
    std::string selfIncludeStr;

public:
    FileContext(std::string &filepath, OptionHandler &opts);

    ~FileContext();

    void initializeOutputStreams(std::string const &filepath);

    void writeScopeChanges(std::ofstream &fout, size_t indentCurr, size_t indentPrev);

    void handleStructKeyword();
    void handleClassKeyword(std::string const &expr);

    void writeExpr(size_t indentCurr, std::string const &currExpr);
    void selfInclude(char const endchar = 0);
};
}

#endif
