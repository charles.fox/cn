#include <string>

#include "Driver.hpp"
#include "Log.hpp"

using namespace cn;

int main(int argc, char *argv[])
{
    OptionHandler opts(argc, argv);

    Driver driver(opts);

    for (int optind = opts.optionIndex; optind < argc; optind++)
    {
        std::string filename(argv[optind]);
        log(Log::Verbose) << "Parsing argument " << optind << ": " << filename << std::endl;
        if (driver.parse(filename))
        {
            log(Log::Critical) << "Could not parse file " << filename << "." << std::endl;
            exit(1);
        }
    }

    log(Log::Verbose) << "Exiting successfully." << std::endl;

    return 0;
}
