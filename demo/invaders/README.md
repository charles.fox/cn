# Invaders CNatural Demo

## Instructions

Left and Right arrow keys to move; Spacebar to fire.

## Dependencies
- SDL2

e.g. on most apt-based systems:
```
apt-get install libsdl2 libsdl2-image
```
or
```
apt-get install libsdl2-dev libsdl2-image-dev
```

## Build
```
cd demo/invaders
make
```

Or build it yourself:
```
cd demo/invaders
../../bin/cn invaders.cn
```
Then create the executable with your favourite C++ compiler, e.g. gcc:
```
g++ -std=c++17 -g invaders.cpp -I"include" -L"lib" -Wall -lSDL2main -lSDL2 -lSDL2_image -o invaders
```

and run the game with
```
./invaders
```
