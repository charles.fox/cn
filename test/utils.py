import subprocess
from constants import cn_dir, cpp_file_dir, cn_file_dir, ref_file_dir
import shutil
import os
import glob
import pytest

@pytest.fixture(scope="module")
def compile_cn(request):
    """Compile CN code and move cpp and hpp files to cnc directory"""
    try:
        for f in glob.glob(cpp_file_dir+'/'+request.param+'.*'):
            os.remove(f)
    except AttributeError:
        return

    if not os.path.isdir(cpp_file_dir):
        os.mkdir(cpp_file_dir)
    subprocess.run([cn_dir, '--output-dir', cpp_file_dir, f'tests/cn_files/{request.param}.cn'])


def compare_cpp_files(file):
    cn_content = ""
    with open(cpp_file_dir + "/" + file) as cn_file:
        cn_content = cn_file.read()

    ref_content = ""
    with open(ref_file_dir+ "/"+ file) as ref_file:
        ref_content = ref_file.read()

    cn_content = cn_content.replace("\n", "").replace("\t", "").replace("    ", " ")
    ref_content = ref_content.replace("\n", "").replace("\t", "").replace("    ", " ")

    print(cn_content)
    print(ref_content)
    assert cn_content in ref_content


def compare_hpp_files(file):
    cn_content = ""
    with open(cpp_file_dir + "/" + file) as cn_file:
        cn_content = cn_file.read()


    ref_content = ""
    with open(ref_file_dir+ "/" + file) as ref_file:
        ref_content = ref_file.read()

    cn_content = cn_content.replace("\n", "").replace("\t", "").replace("    ", " ")
    ref_content = ref_content.replace("\n", "").replace("\t", "").replace("    ", " ")

    print(cn_content)
    print(ref_content)
    assert cn_content == ref_content


def run_cpp(file):
    subprocess.run(["g++", cpp_file_dir + "/" + file], stdout=subprocess.PIPE)
    result = subprocess.run(["./a.out"], stdout=subprocess.PIPE)
    print(result.returncode)
    assert result.returncode == 0

    os.remove('a.out')
