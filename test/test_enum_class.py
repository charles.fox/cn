from utils import compile_cn, compare_cpp_files, compare_hpp_files, run_cpp
import pytest

@pytest.mark.parametrize('compile_cn', ['EnumClass'], indirect=['compile_cn'])
def test_cpp(compile_cn):
    compare_cpp_files('EnumClass.cpp')

@pytest.mark.parametrize('compile_cn', ['EnumClass'], indirect=['compile_cn'])
def test_hpp(compile_cn):
    compare_hpp_files('EnumClass.hpp')

@pytest.mark.parametrize('compile_cn', ['EnumClass'], indirect=['compile_cn'])
def test_code(compile_cn):
    run_cpp('EnumClass.cpp')