#include "Function.hpp"

int ExampleFunc(int a)
{
	a += 5;
	return a * 3;
}

int main()
{
	ExampleFunc(1);
}