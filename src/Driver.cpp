#include "Driver.hpp"

using namespace cn;

Driver::Driver(OptionHandler opts) :
    _opts(opts),
    _lexer(*this),
    _parser(*this, _fileContext)
{
    // Note: When doing a trace on flex, you may see end of buffer or NUL
    // warnings for each character matched. It's unclear why this is the case,
    // but it appears to be a problem with the default flex class.
    // At any rate, it has no effect on lexer output.
    _lexer.set_debug(_opts.traceLexer());
    _parser.set_debug_level(_opts.traceParser());
}

int Driver::parse(std::string &filename)
{
    log(Log::Verbose) << "Opening " << filename << " for reading..." << std::endl;
    std::ifstream fin(filename);

    log(Log::Verbose) << "Checking " << filename << "..." << std::endl;
    if (!fin.good())
    {
        log(Log::Critical) << filename << " not readable." << std::endl;
        return 1;
    }

    _lexer.switch_streams(&fin);
    _fileContext = new FileContext(filename, _opts);

    log(Log::Verbose) << "Beginning parsing..." << std::endl;
    int out = _parser.parse();

    fin.close();
    delete _fileContext;

    return out;
}
